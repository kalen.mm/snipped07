FROM golang
ADD cmd /go/src/snipped07/
ENV POSTGRES_USER: admin
ENV POSTGRES_PASSWORD: 562724mmm
ENV POSTGRES_DB: snippet07
RUN mkdir -p /go/src/snipped07/
COPY . /go/src/snipped07/
WORKDIR /go/src/snipped07/
ENTRYPOINT /go/src/snipped07/app

#FROM alpine:latest
#RUN apk --no-cache add ca-certificates
#WORKDIR /go/src/snipped07app .
CMD ["./go/src/snipped07/"]
